﻿CREATE TABLE [dbo].[Bookings]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] VARCHAR(50) NOT NULL, 
    [TimeStamp] DATETIME NOT NULL,
    [HotelId] INT NOT NULL, 
    CONSTRAINT [FK_Bookings_ToHotels] FOREIGN KEY ([HotelId]) REFERENCES [Hotels]([Id]), 
)
GO

CREATE INDEX [IX_Bookings_HotelId] ON [dbo].[Bookings] ([HotelId])

