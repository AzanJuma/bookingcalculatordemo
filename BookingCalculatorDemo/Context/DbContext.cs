﻿
namespace BookingCalculatorDemo.Context
{
    using System.Configuration;
    using System.Data.SqlClient;

    public class DbContext
    {
        public static SqlConnection GetSqlConnection()
        {
            return new SqlConnection(GetSqlConnectionString);
        }

        private static string GetSqlConnectionString
        {
            get
            {
                return SqlStringBuilder("SqlString").ToString();
            }
        }

        private static SqlConnectionStringBuilder SqlStringBuilder(string connString)
        {
            var connBuilder = new SqlConnectionStringBuilder()
            {
                AsynchronousProcessing = true,
                ConnectionString = ConfigurationManager.AppSettings[connString],
                ConnectRetryCount = 3,
                ConnectRetryInterval = 30,
                MultipleActiveResultSets = true,
                TransactionBinding = "Explicit Unbind"
            };

            return connBuilder;
        }
    }
}