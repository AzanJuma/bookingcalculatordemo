﻿
namespace BookingCalculatorDemo.Models
{
    using System.Collections.Generic;

    public class Hotel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<Guest> GuestList { get; set; }
        public int Capacity { get; set; }
        public bool IsFull { get; set; }
    }
}