﻿
namespace BookingCalculatorDemo.Models
{
    using System;

    public class Payment
    {
        public int GuestId { get; set; }
        public int HotelId { get; set; }
        public decimal PaymentAmount { get; set; }
        public DateTime PaymentDate { get; set; }
    }
}