﻿
namespace BookingCalculatorDemo.Models
{
    using System;

    public class Guest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int HotelId { get; set; }
        public DateTime BookingDate { get; set; }
    }
}