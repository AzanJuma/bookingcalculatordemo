﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Version 1.0.0
//     Last Updated 2017-11-07
//
//     This code was generated from a template using TypeWriter Extension.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
//
//     Copyrighted. Softinn Solutions Sdn. Bhd. All rights reserved.
// </auto-generated>
//------------------------------------------------------------------------------
$Classes(c => c.Namespace.Equals(sourceProjectNamespace) 
            && c.Attributes.Any(a => a.Value.Equals("TypeWriterIgnore", StringComparison.OrdinalIgnoreCase)) == false
            && c.Name.Equals("C__RefactorLogViewModel", StringComparison.OrdinalIgnoreCase) == false)[
'use strict';

namespace $GetNamespace {
    export interface $GetInterfaceName { $Properties[
        $Type[$IsObject[//$Name][$Parent: $Name;]]]
    }

    export class $GetClassName implements $GetInterfaceName { $Properties[
        $Type[$IsObject[//$Name][public $Parent: $Name;]]]

        constructor($Properties[
            $Type[$IsObject[//$Name][$Parent: $Name,]]]
        ){$Properties[
                $Type[$IsObject[//$Name][this.$Parent = $Parent;]]]            
        }
    }
}]


${    
    const string sourceProject = "BookingCalculator.Data";
    const string sourceProjectNamespace = sourceProject +  ".ViewModel";

    Template(Settings settings)
    {
        settings.IncludeProject(sourceProject);       
        settings.OutputFilenameFactory = file => 
        {
            return $"{file.Name.Replace(".cs", ".ts").Replace("ViewModel", "")}";
        };
    }

    bool IsObject(Type selectedType){
        return (selectedType.Name.Contains("ViewModel"));
    }

    string GetNamespace(Class selectedClass)
    {
        return sourceProject.ToLower();
    }   

    string GetClassName(Class selectedClass)
    {
        return selectedClass.Name.Replace("ViewModel", "");
    }

    string GetInterfaceName(Class selectedClass) 
    {
        return "I" + GetClassName(selectedClass);
    }
}



