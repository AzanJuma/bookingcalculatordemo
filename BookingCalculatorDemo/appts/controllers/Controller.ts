﻿namespace bookingcalculator.controllers {
    'use strict';

    export interface IController<T> {
        viewmodels: T[];
        viewmodel: T;

        isLoading: boolean;
        loadingMessage: string;
        isOnEditMode: boolean;

        startLoading(loadingMessage: string): void;
        stopLoading(): void;
    }

    export class Controller<T> implements IController<T> {
        logger: softinn.services.Logger;
        viewmodels: T[];
        viewmodel: T;

        isLoading: boolean;
        loadingMessage: string;
        isOnEditMode: boolean;

        static $inject: Array<string> = [
            'Logger',
        ];
        constructor(
            logger: softinn.services.Logger
        ) {
            this.logger = logger;

            this.viewmodels = new Array<T>();
            this.viewmodel = <T>{};

            this.isLoading = false;
            this.loadingMessage = '';
            this.isOnEditMode = false;
        }

        // Watch Events

        // UI Events
        onAddRecordClick(): void {
            this.viewmodel = <T>{};
            this.isOnEditMode = false;
        }

        onEditRecordClick(selectedViewModel: T): void {
            let cloneRecord = angular.copy(selectedViewModel);
            this.onViewModelSelected(cloneRecord);
            this.isOnEditMode = true;
        }

        onViewModelSelected(selectedViewModel: T): void {
            this.viewmodel = selectedViewModel;
        }

        // UI Support Functions
        startLoading(loadingMessage: string): void {
            this.loadingMessage = loadingMessage;
            this.isLoading = true;
        }

        stopLoading(): void {
            this.loadingMessage = '';
            this.isLoading = false;
        }

        // Support Functions
        refreshViewModels<T extends { Id: number }>(arg: T, args: T[]): void {
            var filteredArgs = args.filter(argInstance => argInstance.Id == arg.Id);
            if (filteredArgs.length == 0) {
                args.push(arg);
            }
            if (filteredArgs.length == 1) {
                angular.forEach(args,
                    (argInstance, index) => {
                        if (argInstance.Id === arg.Id) {
                            args[index] = angular.copy(arg);
                        }
                    });
            }

        }

        arrayOfObjectIndexOf<T extends { Id: number }>(args: T[], arg: T): number {
            for (var i = 0; i < args.length; i++) {
                if (angular.equals(args[i], arg)) {
                    return i;
                }
            }
        }

        resetViewModel() {
            this.viewmodel = <T>{};
        }

        resetViewModels() {
            this.viewmodels = new Array<T>();
        }
        // Listening Events

        // Database Read

        // == Default Sections
        // Watch Events
        // UI Events
        // UI Support Functions
        // Support Functions
        // Listening Events
        // Database Read
        // Database CRUD
    }
}