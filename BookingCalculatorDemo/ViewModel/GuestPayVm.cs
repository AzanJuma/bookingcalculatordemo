﻿
namespace BookingCalculatorDemo.ViewModel
{
    using System;
    using System.Threading.Tasks;

    using BookingCalculatorDemo.Context;
    using BookingCalculatorDemo.Models;

    using Dapper;

    public class GuestPayVm
    {
        public string Name { get; set; }
        public string HotelName { get; set; }
        public decimal Fee { get; set; }
        public decimal Comission { get; set; }

        /// <summary>
        /// Insert Payment Order Into Database
        /// </summary>
        /// <param name="guestPay">
        /// The guest pay.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        internal static async Task<int> CreatePayment(GuestPayVm guestPay)
        {
            var guest = await GuestVm.GetCustomerInfo(guestPay.Name);
            var hotel = await HotelVm.GetSpecificHotel(guestPay.HotelName);
            var payout = GetPayout(guestPay.Fee, guestPay.Comission);

            using (var conn = DbContext.GetSqlConnection())
            {
                var affectedRow = conn.Execute(
                    "INSERT INTO dbo.Payment(GuestId,HotelId,PaymentAmount,PaymentDate) VALUES (@GuestId,@HotelId,@PaymentAmount,@PaymentDate)",
                    new Payment
                        {
                            GuestId = guest.Id,
                            HotelId = hotel.Id,
                            PaymentAmount = payout,
                            PaymentDate = DateTime.Now
                        });

                return affectedRow;
            }
        }

        private static decimal GetPayout(decimal fee, decimal comission)
        {
            return (fee - (fee * comission));
        }
    }
}