﻿
namespace BookingCalculatorDemo.ViewModel
{
    using System.Linq;
    using System.Threading.Tasks;

    using BookingCalculatorDemo.Context;
    using BookingCalculatorDemo.Models;

    using Dapper;

    public class GuestVm
    {
        internal static async Task<Guest> GetCustomerInfo(int id)
        {
            using (var conn = DbContext.GetSqlConnection())
            {
                conn.Open();
                var result = await conn.QueryAsync<Guest>("Select * from dbo.Guest Where Id = @Id", new { Id = id });

                return result.FirstOrDefault();
            }
        }

        internal static async Task<Guest> GetCustomerInfo(string name)
        {
            using (var conn = DbContext.GetSqlConnection())
            {
                conn.Open();
                var result = await conn.QueryAsync<Guest>("Select * from dbo.Guest Where Name = @Name", new { Name = name });

                return result.FirstOrDefault();
            }
        }
    }
}