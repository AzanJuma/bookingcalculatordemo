﻿
namespace BookingCalculatorDemo.ViewModel
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using BookingCalculatorDemo.Context;
    using BookingCalculatorDemo.Models;

    using Dapper;

    public class HotelVm
    {
        public string Name { get; set; }
        public int Capacity { get; set; }

        public static async Task<IEnumerable<HotelVm>> GetAllHotels()
        {
            using (var conn = DbContext.GetSqlConnection())
            {
                conn.Open();
                var hotels = await conn.QueryAsync<HotelVm>("Select * from dbo.Hotel");
                
                return hotels;
            }
        }

        public static async Task<Hotel> GetSpecificHotel(string name)
        {
            using (var conn = DbContext.GetSqlConnection())
            {
                conn.Open();
                var hotel = await conn.QueryAsync<Hotel>("Select * from dbo.Hotel where Name = @Name", new { Name = name });

                return hotel.FirstOrDefault();
            }
        }
    }
}