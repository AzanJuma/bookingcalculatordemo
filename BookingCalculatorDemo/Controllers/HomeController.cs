﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookingCalculatorDemo.Controllers
{
    using BookingCalculatorDemo.ViewModel;

    public class HomeController : Controller
    {
        public ActionResult Index(int customerId = 2)
        {
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}
