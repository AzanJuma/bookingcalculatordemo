﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookingCalculator.Data.Repository;

namespace BookingCalculator.Data
{
    public class Database
    {
        private DatabaseContext _databaseContext = new DatabaseContext();

        public Database()
        {
            _databaseContext.Configuration.ProxyCreationEnabled = true;
            _databaseContext.Configuration.LazyLoadingEnabled = true;
        }

        public Database(string connectionString)
        {
            _databaseContext = new DatabaseContext(connectionString);
        }

        public Database(DatabaseContext dbContext)
        {
            _databaseContext = dbContext;
        }

        public BookingRepository Bookings
        {
            get { return new BookingRepository(_databaseContext); }
        }

        public HotelRepository Hotels
        {
            get { return new HotelRepository(_databaseContext); }
        }

        public void Save()
        {
            _databaseContext.SaveChanges();
        }
    }
}
