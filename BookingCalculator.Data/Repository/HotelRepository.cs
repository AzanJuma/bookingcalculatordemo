﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingCalculator.Data.Repository
{
    public class HotelRepository : Repository<Hotel>
    {
        public HotelRepository(DatabaseContext dbContext) : base(dbContext)
        {
        }
    }
}
