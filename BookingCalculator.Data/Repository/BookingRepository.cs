﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingCalculator.Data.Repository
{
    public class BookingRepository: Repository<Booking>
    {
        public BookingRepository(DatabaseContext dbContext) : base(dbContext)
        {
        }
    }
}
