﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingCalculator.Data.CustomViewModels
{
    public class BookingViewModel : ViewModel.BookingViewModel
    {
        public bool IsAllowApprove { get; set; }
        public DateTime? PaymentDueDate { get; set; }
        public BookingViewModel()
        {

        }

        public BookingViewModel(Booking BookingValue) : base(BookingValue)
        {

        }
    }
}
